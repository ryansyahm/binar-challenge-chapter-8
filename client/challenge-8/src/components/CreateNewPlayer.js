import { useState } from "react";

const CreateNewPlayer = (props) => {
  // function for showing and hide password
  const showPassword = () => {
    var x = document.getElementById("password");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  };
  // state management for email username and password
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [exp, setExp] = useState("");

  const submitHandler = (event) => {
    event.preventDefault();
    // make new player object 
    const newPlayer = {
      email: email,
      username: username,
      password: password,
      experience: exp,
      level: Math.floor(exp/1000)
    };
    // pass data to parrent
    props.onCreateSubmit(newPlayer);
    // set state to empty strings after submit
    setEmail("");
    setUsername("");
    setPassword("");
    setExp("");
  };
  return (
    <div className="mt-5" id="add">
      <div>
        <h1 className="text-center mt-5">Add New Player</h1>
      </div>
      <form onSubmit={submitHandler}>
        <div className="mb-3">
          <label htmlFor="email" className="form-label">
            Email
          </label>
          <input
            className="form-control"
            type="email"
            name="email"
            placeholder="email@mail.com"
            id="email"
            // change email state
            onChange={(e) => setEmail(e.target.value)}
            value={email}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="username" className="form-label">
            Username
          </label>
          <input
            className="form-control"
            type="text"
            name="username"
            placeholder="enter your username"
            id="username"
            // change username state
            onChange={(e) => setUsername(e.target.value)}
            value={username}
            autoComplete="username"
          />
        </div>
        <div className="mb-3">
          <label htmlFor="password" className="form-label">
            Password
          </label>
          <input
            className="form-control"
            type="password"
            name="password"
            placeholder="enter your password"
            id="password"
            // change password state
            onChange={(e) => setPassword(e.target.value)}
            value={password}
            autoComplete="current-password"
          />
          <div>
            <input
              className="d-inline me-1"
              type="checkbox"
              onClick={showPassword}
            />
            <p className="d-inline">Show Password</p>
          </div>
        </div>
        <div className="mb-3">
          <label htmlFor="experience" className="form-label">
            Experience Point
          </label>
          <input
            className="form-control"
            type="number"
            name="exp"
            placeholder="Enter the experience point"
            id="experience"
            // change exp state
            onChange={(e) => setExp(e.target.value)}
            value={exp}
          />
        </div>
        <button className="btn btn-primary" type="submit">
          Submit
        </button>
      </form>
    </div>
  );
};

export default CreateNewPlayer;
