import { useState } from 'react'
import PlayerList from "./SearchBarComponents/PlayerLists";
const SearchBar = (props) => {
  // state for inputed search value
  const [searchValue, setSearchValue] = useState('')
  // filter array from items props and filter it using searchvalue state
  const filteredSearch = props.items.filter((item) => {
    return item.username.includes(searchValue)
  })
  // set value of searchvalue
  const filterHandler = (event) => {
    setSearchValue(event.target.value)
  }

  return (
    <div>
      <h1 className="mt-5 mb-3 text-center">Search</h1>
      {/* add handler function onchange */}
      <input className="form-control mb-3" type="text" placeholder="Enter Player username" onChange={filterHandler} />
      {/* pass filtered array to playerlist component */}
      <PlayerList searchValue={filteredSearch} />
    </div>
  )
}

export default SearchBar