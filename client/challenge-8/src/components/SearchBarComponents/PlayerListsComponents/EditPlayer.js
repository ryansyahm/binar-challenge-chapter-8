import { useState } from "react";

const EditPlayer = ({ items, index }) => {
  // set state for item props and index
  const [newItem, setNewItem] = useState({...items});
  return (
    // modal for editing player
    <div key={index}>
      <button
        type="button"
        className="btn btn-warning"
        data-bs-toggle="modal"
        data-bs-target={`#id${index}`}
      >
        Edit
      </button>
      <div
        className="modal fade"
        id={`id${index}`}
        tabIndex="-1"
        aria-labelledby={`id${index}`}
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id={`id${index}`}>
                Modal title
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <div className="mb-3">
                <label htmlFor="email" className="form-label">
                  Email
                </label>
                <input
                  className="form-control"
                  type="email"
                  name="email"
                  placeholder="email@mail.com"
                  id="email"
                  defaultValue={newItem.email}
                  onChange={event => {setNewItem(event.target.value)}}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="username" className="form-label">
                  Username
                </label>
                <input
                  className="form-control"
                  type="text"
                  name="username"
                  placeholder="enter your username"
                  id="username"
                  defaultValue={newItem.username}
                  autoComplete="username"
                />
              </div>
              <div className="mb-3">
                <label htmlFor="password" className="form-label">
                  Password
                </label>
                <input
                  className="form-control"
                  type="password"
                  name="password"
                  placeholder="enter your password"
                  id="password"
                  defaultValue={newItem.password}
                  autoComplete="current-password"
                />
              </div>
              <div className="mb-3">
                <label htmlFor="experience" className="form-label">
                  Experience Point
                </label>
                <input
                  className="form-control"
                  type="number"
                  name="exp"
                  placeholder="Enter the experience point"
                  id="experience"
                  defaultValue={newItem.experience}
                />
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-bs-dismiss="modal"
              >
                Close
              </button>
              <button
                type="button"
                className="btn btn-primary"
              >
                Edit
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditPlayer;
