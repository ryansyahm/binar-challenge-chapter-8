import EditPlayer from './EditPlayer'

const TableData = ({ item, index }) => {
  // render all player list
  return (
    <tr key={index}>
      <th scope="row">{index}</th>
      <td>{item.email}</td>
      <td>{item.username}</td>
      <td>{item.password}</td>
      <td>{item.experience}</td>
      <td>{item.level}</td>
      <td key={index}><EditPlayer items={item} index={index} /></td>
    </tr>
  )

}

export default TableData