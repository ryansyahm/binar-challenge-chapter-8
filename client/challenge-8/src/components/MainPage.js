import {useState} from 'react';
import CreateNewPlayer from "./CreateNewPlayer";
import SearchBar from './SearchBar'

const MainPage = () => {
  // use state for changing player list value
  const [playerList, setPlayerList] = useState([])

  // passing data from child to parrent function
  const addNewPlayer = (newPlayer) => {
    setPlayerList([...playerList, newPlayer])
  }
  return (
    <div>
      {/* pass function as a props to children to lift up state */}
      <CreateNewPlayer onCreateSubmit={addNewPlayer}/>
      {/* pass playerlist props to searchbar */}
      <SearchBar items={playerList}/>
    </div>
  );
};

export default MainPage;
