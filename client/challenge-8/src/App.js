// import mainpage and navbar component
import MainPage from "./components/MainPage";
import Navbar from "./components/Navbar";

function App() {
  return (
    <div>
      {/* insert navbar component */}
      <Navbar />
      <div className="container-fluid align-center">
        {/* insert mainpage component */}
        <MainPage />
      </div>
    </div>
  );
}

export default App;
